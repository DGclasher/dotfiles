#!/bin/zsh

# Add some extras to $PATH
export PATH="$PATH:$HOME/.local/bin:$HOME/.local/share/cargo/bin"

unsetopt PROMPT_SP

# Default programs
export EDITOR="vim"
export BROWSER="brave-browser"

# Home directory cleanup
# Setting variables
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XINITRC="$XDG_CONFIG_HOME/x11/xinitrc"

# Default paths for applications
export MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc"
export VIMINIT='source $MYVIMRC'
export GOPATH="$XDG_DATA_HOME/go"
export PM2_HOME="$XDG_DATA_HOME/pm2"
export UNISON="$XDG_DATA_HOME/unison"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export KODI_DATA="$XDG_DATA_HOME/kodi"
export WINEPREFIX="$XDG_DATA_HOME/wine"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUBY_HOME="$XDG_CONFIG_HOME/ruby"
export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
export ELECTRUMDIR="$XDG_DATA_HOME/electrum"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export MBSYNCRC="$XDG_CONFIG_HOME/mbsync/config"
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch-config"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"
export ANSIBLE_CONFIG="$XDG_CONFIG_HOME/ansible/ansible.cfg"
#export GNUPGHOME="$XDG_DATA_HOME/gnupg"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.

# History files
export HISTFILE="$XDG_DATA_HOME/history"
export LESSHISTFILE="$XDG_CACHE_HOME/.lesshst"
export GDBHISTFILE="$XDG_CACHE_HOME/.gdb_history"
export PSQL_HISTORY="$XDG_CACHE_HOME/.psql_history"
export MYSQL_HISTFILE="$XDG_CACHE_HOME/.mysql_history"
export SQLITE_HISTORY="$XDG_CACHE_HOME/.sqlite_history"
export REDISCLI_HISTFILE="$XDG_CACHE_HOME/.redis_history"
export NODE_REPL_HISTORY="$XDG_CACHE_HOME/.node_repl_history"

# Other program settings:
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"
export MOZ_USE_XINPUT2="1" # Mozilla smooth scrolling/touchpads.
